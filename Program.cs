﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstLesson
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();
            Player value = new Player("Ywnd", rnd.Next(1,101));
            value.ConsoleWriteline();
            Console.ReadKey();
        }
    }
    class Player
    {
        string nickname;
        int health;

        public Player(string nickname, int health)
        {
            this.nickname = nickname;
            this.health = health;
        }

        public void ConsoleWriteline()
        {
            Console.WriteLine($"Логин:    {this.nickname}");
            Console.WriteLine($"Здоровье: {this.health}/100");
        }
    }
}
